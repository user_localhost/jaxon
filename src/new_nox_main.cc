#include "config.h"

#include "nox.hh"

using namespace vigil;

int libmain(int, char **);

int main(int argc, char *argv[]) {
	return libmain(argc, argv);
}
