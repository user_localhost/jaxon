/* Copyright 2011-2012 (C) OSSS lab.
 *
 * Author : Hiroyasu OHYAMA (OSSS lab. and ariel-network Inc.)
 *
 * Description : 
 *	This NOX component only works in cooperating with jaxoncore library.
 *	And the processing of libjaxoncore is executed Java program through JNA
 *	(http://jna.java.net).
 * */

#include "jaxon.hh"

#include <boost/bind.hpp>
#include <boost/shared_array.hpp>
#include <boost/thread.hpp>

/*
 * For using inet_aton
 * */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "nox.hh"
#include "flow-mod-event.hh"
#include "datapath-join.hh"
#include "datapath-leave.hh"
#include "packet-in.hh"
#include "table-stats-in.hh"
#include "flow-stats-in.hh"
#include "port-stats-in.hh"
#include "aggregate-stats-in.hh"

#include "error-event.hh"
#include "assert.hh"
#include "openflow-default.hh"
#include "packets.h"

#include <iostream>
#include <stdio.h>

using namespace vigil;

static Vlog_module lg("jaxon");

static struct request *get_request(void);
static struct table_stats *get_table_stats(const Table_stats_in_event *);
static struct port_stats *get_port_stats(const Port_stats_in_event *);
static struct aggregate_stats *get_aggregate_stats(const Aggregate_stats_in_event *);
static struct flow_stats *get_flow_stats(const Flow_stats_in_event &);
static void put_request(struct request *);
static void put_request_data(struct table_stats *);
static void put_request_data(struct port_stats *);
static void put_request_data(struct flow_stats *);
static int init_ofp(ofp_flow_mod *);

void jaxon::configure(const Configuration* c) {
	register_handler<Datapath_join_event>
			(boost::bind(&jaxon::handle_datapath_join, this, _1));
	register_handler<Datapath_leave_event>
			(boost::bind(&jaxon::handle_datapath_leave, this, _1));
	register_handler<Packet_in_event>
			(boost::bind(&jaxon::handle_packet_in, this, _1));
	register_handler<Error_event>
			(boost::bind(&jaxon::handle_error, this, _1));
	register_handler<Table_stats_in_event>
			(boost::bind(&jaxon::handle_table_stats_in, this, _1));
	register_handler<Flow_stats_in_event>
			(boost::bind(&jaxon::handle_flow_stats_in, this, _1));
	register_handler<Port_stats_in_event>
			(boost::bind(&jaxon::handle_port_stats_in, this, _1));
	register_handler<Aggregate_stats_in_event>
			(boost::bind(&jaxon::handle_aggregate_stats_in, this, _1));

	//memset(&event_handlers, NULL, sizeof(struct openflow_operations));
	
	//fsm.start(boost::bind(&jaxon::thread_foo, this));
}

void jaxon::thread_foo() {
	if(g_dpid.as_host() > 0) {
		//send_table_stats_command(g_dpid);
		send_table_stats_request(g_dpid);
	}

	co_timer_wait(do_gettimeofday() + timeval_from_ms(500), NULL);
	co_fsm_block();
}

void jaxon::install() {
	/* initializing global attrs */
	g_dpid = datapathid();

	//thread.start(boost::bind(&jaxon::thread_foo, this));
}

void jaxon::getInstance(const Context* c, jaxon*& component) {
	component = dynamic_cast<jaxon*>
		(c->get_by_interface(container::Interface_description
					(typeid(jaxon).name())));
}

Disposition jaxon::handle_datapath_join(const Event& e) {
	const Datapath_join_event& dj = assert_cast<const Datapath_join_event&>(e);
	Disposition ret = CONTINUE;

	if(event_handlers.datapath_join != NULL) {
		struct request *req = get_request();

		if(req != NULL) {
			req->datapath_id = dj.datapath_id;

			ret = (Disposition) event_handlers.datapath_join(req);

			put_request(req);
		} else {
			printf("[handle_datapath_join] (ERROR) fail to get memory\n");
		}
	}

	return ret;
}

Disposition jaxon::handle_datapath_leave(const Event& e) {
	const Datapath_leave_event& dj = assert_cast<const Datapath_leave_event&>(e);
	Disposition ret = CONTINUE;

	if(event_handlers.datapath_leave != NULL) {
		struct request *req = get_request();

		if(req != NULL) {
			req->datapath_id = dj.datapath_id;

			ret = (Disposition) event_handlers.datapath_leave(req);

			put_request(req);
		} else {
			printf("[handle_datapath_leave] (ERROR) fail to get memory\n");
		}
	}

	return ret;
}

Disposition jaxon::handle_table_stats_in(const Event& e) {
	const Table_stats_in_event& ts = assert_cast<const Table_stats_in_event&>(e);
	Disposition ret = CONTINUE;

	if(event_handlers.table_stats_in != NULL) {
		struct request *req = get_request();
		struct table_stats *tsobj = get_table_stats(&ts);

		if(req != NULL && tsobj != NULL) {
			req->datapath_id = ts.datapath_id;
			req->data = (void *) tsobj;

			ret = (Disposition) event_handlers.table_stats_in(req);
		}

		put_request_data(tsobj);
		put_request(req);
	}

	return ret;
}

Disposition jaxon::handle_port_stats_in(const Event& e) {
	const Port_stats_in_event& ps = assert_cast<const Port_stats_in_event&>(e);
	Disposition ret = CONTINUE;
	
	if(event_handlers.port_stats_in != NULL) {
		struct request *req = get_request();
		struct port_stats *psobj = get_port_stats(&ps);

		if(req != NULL && psobj != NULL) {
			req->datapath_id = ps.datapath_id;
			req->data = (void *) psobj;

			ret = (Disposition) event_handlers.port_stats_in(req);
		}

		put_request_data(psobj);
		put_request(req);
	}

	return ret;
}

Disposition jaxon::handle_aggregate_stats_in(const Event& e) {
	const Aggregate_stats_in_event& as = assert_cast<const Aggregate_stats_in_event&>(e);
	Disposition ret = CONTINUE;
	
	if(event_handlers.aggregate_stats_in != NULL) {
		struct request *req = get_request();
		struct aggregate_stats *asobj = get_aggregate_stats(&as);

		if(req != NULL && asobj != NULL) {
			req->datapath_id = as.datapath_id;
			req->data = (void *) asobj;

			ret = (Disposition) event_handlers.aggregate_stats_in(req);
		}

		put_request(req);
	}

	return ret;
}

Disposition jaxon::handle_flow_stats_in(const Event& e) {
	const Flow_stats_in_event& fs = assert_cast<const Flow_stats_in_event&>(e);
	Disposition ret = CONTINUE;
	
	if(event_handlers.flow_stats_in != NULL) {
		struct request *req = get_request();
		struct flow_stats *fsobj = get_flow_stats(fs);

		if(req != NULL && fsobj != NULL) {
			req->datapath_id = fs.datapath_id;
			req->data = (void *) fsobj;

			ret = (Disposition) event_handlers.flow_stats_in(req);
		}

		put_request_data(fsobj);
		put_request(req);
	}

	return ret;
}

Disposition jaxon::handle_packet_in(const Event& e) {
	const Packet_in_event& pie = assert_cast<const Packet_in_event&>(e);
	Disposition ret = CONTINUE;

	if(event_handlers.packet_in != NULL) {
		struct request *req = get_request();
		const Flow *flow = &pie.flow;

		if(req != NULL) {
			req->data = (void *) calloc(1, sizeof(vigil::Flow));

			req->ofm->match.in_port = flow->in_port;
			req->ofm->match.dl_type = flow->dl_type;
			req->ofm->match.nw_proto = flow->nw_proto;
			req->ofm->match.nw_src = flow->nw_src;
			req->ofm->match.nw_dst = flow->nw_dst;
			req->ofm->match.tp_src = flow->tp_src;
			req->ofm->match.tp_dst = flow->tp_dst;

			/* copy L2 addr from Flow object to ofp_flow_mod object */
			memcpy((uint8_t *)req->ofm->match.dl_src, (uint8_t *)flow->dl_src.octet, OFP_ETH_ALEN);
			memcpy((uint8_t *)req->ofm->match.dl_dst, (uint8_t *)flow->dl_dst.octet, OFP_ETH_ALEN);
			memcpy((uint8_t *)req->data, (uint8_t *)flow, sizeof(Flow));

			req->datapath_id = pie.datapath_id;
			req->buffer_id = pie.buffer_id;
			req->buf = pie.buf;

			ret = (Disposition) event_handlers.packet_in(req);

			/* release request object */
			put_request(req);
		} else {
			printf("[handle_packet_in] (ERROR) fail to get memory\n");
		}
	}

	return ret;
}

Disposition jaxon::handle_error(const Event& e)
{
	const Error_event& err = assert_cast<const Error_event&>(e);
	Disposition ret = CONTINUE;

	if(event_handlers.error != NULL) {
		struct request *req = get_request();
		struct error_event *ee_obj = (struct error_event *) calloc(1, sizeof(struct error_event));

		if(req != NULL && ee_obj != NULL) {
			ee_obj->type = err.type;
			ee_obj->code = err.code;

			req->datapath_id = err.datapath_id;
			req->data = (void *) ee_obj;

			ret = (Disposition) event_handlers.error(req);
		}

		put_request(req);
	}

	return ret;
}

void jaxon::send_table_stats_request(const datapathid dpid) {
	send_stats_request(dpid, OFPST_TABLE, 0, 0);
}

void jaxon::send_flow_stats_request(const datapathid dpid, struct ofp_match& match, uint8_t table_id) {
   ofp_flow_stats_request fsr;
   fsr.table_id = table_id;
   fsr.match    = match;
   fsr.out_port = htons(OFPP_NONE);
   send_stats_request(dpid, OFPST_FLOW, (const uint8_t*)&fsr, sizeof(struct ofp_flow_stats_request));
}

void jaxon::send_aggregate_stats_request(const datapathid dpid, struct ofp_match& match, uint8_t table_id) {
   ofp_aggregate_stats_request asr;
   asr.table_id = table_id;
   asr.match    = match;
   asr.out_port = htons(OFPP_NONE);
   send_stats_request(dpid, OFPST_FLOW, (const uint8_t*)&asr, sizeof(struct ofp_aggregate_stats_request));
}

int jaxon::send_stats_request(const datapathid dpid, ofp_stats_types type, const uint8_t* data, size_t dsize) {
	ofp_stats_request* osr = NULL;
	size_t msize = sizeof(ofp_stats_request) + dsize;
	boost::shared_array<uint8_t> raw_sr(new uint8_t[msize]);

	// Send OFPT_STATS_REQUEST
	osr = (ofp_stats_request*) raw_sr.get();
	osr->header.type    = OFPT_STATS_REQUEST;
	osr->header.version = OFP_VERSION;
	osr->header.length  = htons(msize);
	osr->header.xid     = 0;
	osr->type           = htons(type);
	osr->flags          = htons(0); /* CURRENTLY NONE DEFINED */

	if(data){
		::memcpy(osr->body, data, dsize );
	}

	//int error = c->send_openflow_command(datapathid::from_host(dpid), &osr->header, false);
	return send_openflow_command(dpid, &osr->header, false);
}

static struct request *get_request() {
	struct request *req = (struct request *) calloc(1, sizeof(struct request));

	if(req != NULL) {
		req->ofm = (ofp_flow_mod *) calloc(1, sizeof(ofp_flow_mod));
		req->data = NULL;

		/* initialize attrs */
		req->buffer_id = -1;

		if(req->ofm != NULL) {
			init_ofp(req->ofm);
		} else {
			free(req->ofm);
			free(req);

			req = NULL;
		}
	}

	return req;
}

static struct table_stats *get_table_stats(const Table_stats_in_event *ts) {
	struct table_stats *ts_obj = (struct table_stats *) calloc(1, sizeof(struct table_stats));

	if(ts_obj != NULL) {
		int size = ts->tables.size();

		ts_obj->ts_array = (struct _table_stats *) calloc(1, sizeof(struct _table_stats) * size);
		ts_obj->length = size;

		if(ts_obj->ts_array != NULL) for(int i=0; i<size; i++) {
			struct _table_stats *lts = &ts_obj->ts_array[i];
			Table_stats obj = ts->tables.at(i);

			lts->name = obj.name.data();
			lts->table_id = obj.table_id;
			lts->max_entries = obj.max_entries;
			lts->active_count = obj.active_count;
			lts->lookup_count = obj.lookup_count;
			lts->matched_count = obj.matched_count;
		}
	}

	return ts_obj;
}

static struct port_stats *get_port_stats(const Port_stats_in_event *ps) {
	struct port_stats *ps_obj = (struct port_stats *) calloc(1, sizeof(struct port_stats));

	if(ps_obj != NULL) {
		int size = ps->ports.size();

		ps_obj->ps_array = (struct _port_stats *) calloc(1, sizeof(struct _port_stats) * size);
		ps_obj->length = size;

		if(ps_obj->ps_array != NULL) for(int i=0; i<size; i++) {
			struct _port_stats *lps = &ps_obj->ps_array[i];
			Port_stats obj = ps->ports.at(i);

			lps->port_no = obj.port_no;
			lps->rx_packets = obj.rx_packets;
			lps->tx_packets = obj.tx_packets;
			lps->rx_bytes = obj.rx_bytes;
			lps->tx_bytes = obj.tx_bytes;
			lps->rx_dropped = obj.rx_dropped;
			lps->tx_dropped = obj.tx_dropped;
			lps->rx_errors = obj.rx_errors;
			lps->tx_errors = obj.tx_errors;
			lps->rx_frame_err = obj.rx_frame_err;
			lps->rx_over_err = obj.rx_over_err;
			lps->rx_crc_err = obj.rx_crc_err;
			lps->collisions = obj.collisions;
		}
	}

	return ps_obj;
}

static struct aggregate_stats *get_aggregate_stats(const Aggregate_stats_in_event *ps) {
	struct aggregate_stats *as_obj = (struct aggregate_stats *)calloc(1, sizeof(struct aggregate_stats));

	if(as_obj != NULL) {
		as_obj->packet_count = ps->packet_count;
		as_obj->byte_count = ps->byte_count;
		as_obj->flow_count = ps->flow_count;
	}

	return as_obj;
}

static struct flow_stats *get_flow_stats(const Flow_stats_in_event& fs) {
	struct flow_stats *fs_obj = (struct flow_stats *) calloc(1, sizeof(struct flow_stats));

	if(fs_obj != NULL) {
		int fs_size = fs.flows.size();

		fs_obj->fs_array = (struct _flow_stats *) calloc(1, sizeof(struct _flow_stats) * fs_size);
		fs_obj->length = fs_size;
		
		if(fs_obj->fs_array != NULL) for(int i=0; i<fs_size; i++) {
			struct _flow_stats *lfs = &fs_obj->fs_array[i];
			Flow_stats obj = fs.flows.at(i);
			int ah_size = obj.v_actions.size();

			lfs->ah_list = (ofp_action_header *)calloc(1, sizeof(ofp_action_header) * ah_size);
			lfs->length = ah_size;

			if(lfs->ah_list != NULL) for(int j=0; j<ah_size; j++) {
				memcpy((uint8_t *)&(lfs->ah_list[j]), (uint8_t *)obj.v_actions.at(j), sizeof(ofp_action_header));
			}
		}
	}

	return fs_obj;
}

static void put_request(struct request *req) {
	if(req != NULL) {
		free(req->ofm);

		if(req->data != NULL) {
			free(req->data);
		}

		free(req);
	}
}

static void put_request_data(struct table_stats *obj) {
	if(obj != NULL) {
		free(obj->ts_array);
	}
}

static void put_request_data(struct port_stats *obj) {
	if(obj != NULL) {
		free(obj->ps_array);
	}
}

static void put_request_data(struct flow_stats *obj) {
	if(obj != NULL) {
		for(int i=0; i<obj->length; i++) if(&obj->fs_array[i] != NULL) {
			free(obj->fs_array[i].ah_list);
		}
		free(obj->fs_array);
	}
}
	
static int init_ofp(ofp_flow_mod *ofm)
{
	ofm->header.version = OFP_VERSION;
	ofm->header.type = OFPT_FLOW_MOD;
	ofm->header.xid = htonl(0);
	ofm->match.dl_vlan = htons(OFP_VLAN_NONE);
	ofm->match.wildcards = 0;
	//ofm->match.dl_type = htons(ETH_TYPE_IP);
	ofm->match.in_port = htons(0);
	ofm->cookie = 0;
	ofm->command = htons(OFPFC_ADD);
	ofm->buffer_id = -1;
	ofm->idle_timeout = htons(0);
	ofm->hard_timeout = htons(0);
	ofm->priority = htons(OFP_DEFAULT_PRIORITY);
	ofm->out_port = htons(OFPP_NONE);
	ofm->flags = htons(ofd_flow_mod_flags());

	return 0;
}

REGISTER_COMPONENT(Simple_component_factory<jaxon>, jaxon);
