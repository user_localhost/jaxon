/* Copyright 2008 (C) Nicira, Inc.
 * Copyright 2009 (C) Stanford University.
 *
 * This file is part of NOX.
 *
 * NOX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NOX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef jaxon_HH
#define jaxon_HH

#include "component.hh"
#include "config.h"
#include "threads/cooperative.hh"

#ifdef LOG4CXX_ENABLED
#include <boost/format.hpp>
#include "log4cxx/logger.h"
#else
#include "vlog.hh"
#endif

struct openflow_operations {
	int (*datapath_join)(struct request *);
	int (*datapath_leave)(struct request *);
	int (*packet_in)(struct request *);
	int (*table_stats_in)(struct request *);
	int (*port_stats_in)(struct request *);
	int (*aggregate_stats_in)(struct request *);
	int (*flow_stats_in)(struct request *);
	int (*error)(struct request *);
};

/* This is intermediate data structure to transform into ofp_action_header and this drivation,
 * and filled by Jaxon-plugin */
struct request {
	ofp_flow_mod *ofm;

	/* Followings are C++ private members that means Java plugin never concern them. */
	vigil::datapathid datapath_id;

	//vigil::Flow *flow;
	void *data;
	uint32_t buffer_id;
	boost::shared_ptr<vigil::Buffer> buf;
};

struct table_stats {
	struct _table_stats *ts_array;
	int length;
};

struct _table_stats {
	int table_id;
	const char *name;
	uint32_t max_entries;
	uint32_t active_count;
	uint64_t lookup_count;
	uint64_t matched_count;
};

struct port_stats {
	struct _port_stats *ps_array;
	int length;
};

struct _port_stats {
	uint16_t port_no;
	uint64_t rx_packets;
	uint64_t tx_packets;
	uint64_t rx_bytes;
	uint64_t tx_bytes;
	uint64_t rx_dropped;
	uint64_t tx_dropped;
	uint64_t rx_errors;
	uint64_t tx_errors;
	uint64_t rx_frame_err;
	uint64_t rx_over_err;
	uint64_t rx_crc_err;
	uint64_t collisions;
};

struct flow_stats {
	struct _flow_stats *fs_array;
	int length;
};

struct _flow_stats {
	ofp_action_header *ah_list;
	int length;
};

struct aggregate_stats {
	uint64_t packet_count;
	uint64_t byte_count;
	uint32_t flow_count;
};

struct flow_action {
	uint16_t type;
	uint16_t vlan_id;
	uint16_t vlan_pcp;
	uint8_t dl_addr[OFP_ETH_ALEN];
	uint16_t dl_type;
	uint8_t nw_proto;
	uint32_t nw_addr;
	uint16_t tp_port;
	uint16_t of_port;
	uint8_t nw_tos;
};

struct error_event {
	uint16_t type;
	uint16_t code;
};

namespace vigil
{
  using namespace std;
  using namespace vigil::container;

	unsigned int jaxon_global_hoge;
	struct openflow_operations event_handlers = {
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
	};

  /** \brief jaxon
   * \ingroup noxcomponents
   * 
   * @author
   * @date
   */
  class jaxon : public Component {
  public:
    /** \brief Constructor of jaxon.
     *
     * @param c context
     * @param node XML configuration (JSON object)
     */
    jaxon(const Context* c, const json_object* node)
      : Component(c)
    {}
    
    /** \brief Configure jaxon.
     * 
     * Parse the configuration, register event handlers, and
     * resolve any dependencies.
     *
     * @param c configuration
     */
    void configure(const Configuration* c);

    /** \brief Start jaxon.
     * 
     * Start the component. For example, if any threads require
     * starting, do it now.
     */
    void install();

    /** \brief Get instance of jaxon.
     * @param c context
     * @param component reference to component
     */
    static void getInstance(const container::Context* c, 
			    jaxon*& component);

		Disposition handle_datapath_join(const Event&);
		Disposition handle_datapath_leave(const Event&);
		Disposition handle_packet_in(const Event&);
		Disposition handle_error(const Event&);
		Disposition handle_table_stats_in(const Event&);
		Disposition handle_flow_stats_in(const Event&);
		Disposition handle_port_stats_in(const Event&);
		Disposition handle_aggregate_stats_in(const Event&);

  private:
		void send_table_stats_request(const datapathid);
		void send_flow_stats_request(const datapathid, struct ofp_match&, uint8_t);
		void send_aggregate_stats_request(const datapathid, struct ofp_match&, uint8_t);
		int send_stats_request(const datapathid, ofp_stats_types, const uint8_t* , size_t);

		void thread_foo();
		Co_fsm fsm;

		datapathid g_dpid;
  };
}

#endif
