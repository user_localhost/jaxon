import java.io.*;
import java.util.*;

import jaxon.*;

/* Copyright 2011-2012 (C) OSSS lab.
 *
 * Author : Hiroyasu OHYAMA (OSSS lab. and ariel-network Inc.)
 *
 * Description:
 *	Here is a sample of Jaxon, that is equivalent to pyswitch which is a sample of NOX written in Python.
 *	
 *
 * */
 
public class Jaswitch {
	public static Jaxon jaxon;
	private static HashMap<byte[], Integer> dir;
	
	private static final byte[] INVALID_L2_ADDR = {-1,-1,-1,-1,-1,-1};

	private static Openflow.Datapathid dpid;

	/* This method will compare l2-addr */
	private static boolean isSame(byte[] a, byte[] b) {
		boolean ret = true;

		for(int i=0; i<Openflow.OFP_ETH_ALEN; i++) {
			if(a[i] != b[i]) {
				ret = false;
				break;
			}
		}

		return ret;
	}

	/* 
	 * This method checks l2-addr which is ditected by argument 
	 * has already had at the directory.
	 *
	 * This returns port number when target addr is found at the directory, or -1.
	 * */
	private static int isinAddr(byte[] addr) {
		Set map_ite = dir.entrySet();
		int ret = -1;

		/* pre-check ditected addr is invalid, or not */
		if(isSame(INVALID_L2_ADDR, addr)) {
			return ret;
		}

		for(Iterator i = map_ite.iterator(); i.hasNext(); ) {
			Map.Entry entry = (Map.Entry) i.next();

			if(isSame((byte[]) entry.getKey(), addr)) {
				ret = (Integer) entry.getValue();

				break;
			}
		}

		return ret;
	}

	/* 
	 * !!! Caution !!!
	 * - This routine never check ditected addr has already been registed in directory. 
	 * */
	private static void setAddr(byte[] addr, short port) {
		dir.put(addr, new Integer(port));
	}

	public static void dol2Learning(Openflow.Flow flow) {
		if(isinAddr(flow.dl_src.octet) < 0) {
			/* regist l2-addr and out-port */
			setAddr(flow.dl_src.octet, flow.in_port);
		}
	}
	
	public static class DPJoinHandler implements Jaxon.CBDatapathJoin {
		public int callback(Jaxon.Request req) {
			dpid = req.dpid;

			return Event.Disposition.CONTINUE;
		}
	}

	public static class PacketInHandler implements Jaxon.CBPacketIn {
		public int callback(Jaxon.RequestPacketIn req) {
			Openflow.Flow flow = req.flow;
			Openflow.OfpFlowMod ofm = req.ofm;

			/* learning MAC-addr and switch-port */
			dol2Learning(flow);

			ofm.match.in_port = flow.in_port;
			int outPort = isinAddr(flow.dl_dst.octet);

			if(outPort >= 0) {
				Jaxon.FlowAction[] attrs = Jaxon.getFlowAction(1);

				attrs[0].type = Openflow.OfpActionType.OFPAT_OUTPUT;
				attrs[0].of_port = (short) outPort;
	
				jaxon.installFlow(req, attrs);
			}

			/* output packet to OFPP_FLOOD which is all output-port except for input-port */
			jaxon.sendPacket(req, Openflow.OfpPort.OFPP_FLOOD);

			return Event.Disposition.CONTINUE;
		}
	}

	public static void main(String[] args) {
		jaxon = new Jaxon();

		/* crate HashMap to work out as a lerning-switch */
		dir = new HashMap<byte[], Integer>();

		jaxon.setHandler(new DPJoinHandler());
		jaxon.setHandler(new PacketInHandler());

		jaxon.run(args);
	}
}
