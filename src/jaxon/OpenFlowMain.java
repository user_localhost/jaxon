import java.io.*;
import java.util.*;

import jaxon.*;

/* Copyright 2011-2012 (C) OSSS lab.
 *
 * Author : Hiroyasu OHYAMA (OSSS lab. and ariel-network Inc.)
 *
 * Description:
 *	Here is a sample of Jaxon.
 *
 *	This controller is experience of changing dst-IP and simple wildcard rule.
 *	Main processing of below is written in datapath-join event handler.
 *	
 * */
 
public class OpenFlowMain {
	public static Jaxon jaxon;
	private static HashMap<byte[], Integer> dir;
	
	private static final byte[] INVALID_L2_ADDR = {-1,-1,-1,-1,-1,-1};

	private static Timer globalTimer;
	private static Openflow.Datapathid dpid;

	/* This method will compare l2-addr */
	private static boolean isSame(byte[] a, byte[] b) {
		boolean ret = true;

		for(int i=0; i<Openflow.OFP_ETH_ALEN; i++) {
			if(a[i] != b[i]) {
				ret = false;
				break;
			}
		}

		return ret;
	}

	/* 
	 * This method checks l2-addr which is ditected by argument 
	 * has already had at the directory.
	 *
	 * This returns port number when target addr is found at the directory, or -1.
	 * */
	private static int isinAddr(byte[] addr) {
		Set map_ite = dir.entrySet();
		int ret = -1;

		/* pre-check ditected addr is invalid, or not */
		if(isSame(INVALID_L2_ADDR, addr)) {
			return ret;
		}

		for(Iterator i = map_ite.iterator(); i.hasNext(); ) {
			Map.Entry entry = (Map.Entry) i.next();

			if(isSame((byte[]) entry.getKey(), addr)) {
				ret = (Integer) entry.getValue();

				break;
			}
		}

		return ret;
	}

	/* 
	 * !!! Caution !!!
	 * - This routine never check ditected addr has already been registed in directory. 
	 * */
	private static void setAddr(byte[] addr, short port) {
		System.out.printf("(Java) [setAddr] %s => %d\n", showl2Addr(addr), port);

		dir.put(addr, new Integer(port));
	}

	public static void dol2Learning(Openflow.Flow flow) {
		if(isinAddr(flow.dl_src.octet) < 0) {
			/* regist l2-addr and out-port */
			setAddr(flow.dl_src.octet, flow.in_port);
		}
	}
	
	public static class DPJoinHandler implements Jaxon.CBDatapathJoin {
		public int callback(Jaxon.Request req) {
			globalTimer = new Timer();

			System.out.printf("(Java) [DPJoinHandler]\n");

			dpid = req.dpid;
			globalTimer.schedule(new StatsGet(req.dpid), 0, 1000);

			return Event.Disposition.CONTINUE;
		}
	}
	
	public static class DPLeaveHandler implements Jaxon.CBDatapathLeave {
		public int callback(Jaxon.Request req) {
			globalTimer = new Timer();

			System.out.printf("(Java) [DPLeaveHandler]\n");

			dpid = req.dpid;
			globalTimer.schedule(new StatsGet(req.dpid), 0, 1000);

			return Event.Disposition.CONTINUE;
		}
	}

	public static class PacketInHandler implements Jaxon.CBPacketIn {
		public int callback(Jaxon.RequestPacketIn req) {
			Openflow.Flow flow = req.flow;
			Openflow.OfpFlowMod ofm = req.ofm;

			System.out.printf("(Java) [PacketInHandler] ofm.in_port: %d (0x%x)\n", ofm.match.in_port, ofm.match.in_port);
			System.out.printf("(Java) [PacketInHandler] flow.in_port: %d (0x%x)\n", flow.in_port, flow.in_port);

			/* learning MAC-addr and switch-port */
			dol2Learning(flow);

			ofm.match.in_port = flow.in_port;
			int outPort = isinAddr(flow.dl_dst.octet);
			
			System.out.printf("(Java) [PacketInHandler] outPort : %d (0x%x)\n", outPort, outPort);

			if(outPort >= 0) {
				Jaxon.FlowAction[] attrs = Jaxon.getFlowAction(1);

				attrs[0].type = Openflow.OfpActionType.OFPAT_OUTPUT;
				attrs[0].of_port = (short) outPort;

				System.out.printf("(Java) [PacketInHandler] call installFlow()\n");
	
				jaxon.installFlow(req, attrs);
			}

			/* output packet to OFPP_FLOOD which is all output-port except for input-port */
			jaxon.sendPacket(req, Openflow.OfpPort.OFPP_FLOOD);

			return Event.Disposition.CONTINUE;
		}
	}
	
	public static class TableStatsInHandler implements Jaxon.CBTableStats {
		public int callback(Jaxon.RequestTableStatsIn req) {
			Jaxon.TableStats tables = req.table;
			Jaxon.TableStatsIn[] lts = tables.toArray();

			System.out.printf("(Java) [TableStatsInHandler] length:%d\n", lts.length);

			for(int i=0; i<lts.length; i++) {
				Openflow.OfpMatch match = new Openflow.OfpMatch();
				System.out.printf("(Java) [TableStatsInHandler] (%d) name:%s (%d)\n", i, lts[i].name, lts[i].table_id);

				match.wildcards = Util.htonl(Openflow.OfpFlowWildcards.OFPFW_ALL);

				Jaxon.send_port_stats_request(dpid, Openflow.OfpPort.OFPP_ALL);
				Jaxon.send_flow_stats_request(dpid, match, (byte)(lts[i].table_id & 0xff));
				Jaxon.send_aggregate_stats_request(dpid, match, (byte)(lts[i].table_id & 0xff));
			}

			return Event.Disposition.CONTINUE;
		}
	}
	
	public static class PortStatsInHandler implements Jaxon.CBPortStats {
		public int callback(Jaxon.RequestPortStatsIn req) {
			Jaxon.PortStats ports = req.port;
			//Jaxon.PortStatsIn[] lps = ports.toArray();

			System.out.printf("(Java) [PortStatsInHandler] len : %d\n", ports.len);

			return Event.Disposition.CONTINUE;
		}
	}
	
	public static class AggregateStatsInHandler implements Jaxon.CBAggregateStats {
		public int callback(Jaxon.RequestAggregateStatsIn req) {
			Jaxon.AggregateStats aggr = req.aggr;

			System.out.printf("(Java) [AggregateStatsInHandler] packet : %d\n", aggr.packet_count);
			System.out.printf("(Java) [AggregateStatsInHandler] byte : %d\n", aggr.byte_count);
			System.out.printf("(Java) [AggregateStatsInHandler] flow : %d\n", aggr.flow_count);
			
			return Event.Disposition.CONTINUE;
		}
	}
	
	public static class FlowStatsInHandler implements Jaxon.CBFlowStats {
		public int callback(Jaxon.RequestFlowStatsIn req) {
			Jaxon.FlowStats flows = req.flow;

			if(flows.len > 0) {
				Jaxon.FlowStatsIn[] lfs = flows.toArray();
	
				System.out.printf("(Java) [FlowStatsInHandler] len : %d\n", flows.len);
	
				for(int i=0; i<lfs.length; i++) {
					Openflow.OfpActionHeader[] fsObj = lfs[i].toArray();
	
					for(int j=0; j<fsObj.length; j++) {
						Openflow.OfpActionHeader action = fsObj[j];
	
						System.out.printf("(Java) [FlowStatsInHandler] (%d, %d)action.type : 0x%x\n", i, j, action.type & 0xffff);
						System.out.printf("(Java) [FlowStatsInHandler] (%d, %d)action.len : 0x%x\n", i, j, action.len & 0xffff);
					}
				}
			}
			
			return Event.Disposition.CONTINUE;
		}
	}

	public static String showl2Addr(byte[] addr) {
		String ret = new String();

		for(int i=0; i<Openflow.OFP_ETH_ALEN; i++) {
			if(i != 0) {
				ret += ":";
			}
			ret += String.format("%02x", addr[i]);
		}

		return ret;
	}

	public static void main(String[] args) {
		jaxon = new Jaxon();

		/* crate HashMap to work out as a lerning-switch */
		dir = new HashMap<byte[], Integer>();

		jaxon.setHandler(new DPJoinHandler());
		jaxon.setHandler(new DPLeaveHandler());
		jaxon.setHandler(new PacketInHandler());
		jaxon.setHandler(new PortStatsInHandler());
		jaxon.setHandler(new FlowStatsInHandler());
		jaxon.setHandler(new TableStatsInHandler());
		jaxon.setHandler(new AggregateStatsInHandler());

		jaxon.run(args);
	}
	
	static class StatsGet extends TimerTask {
		Openflow.Datapathid datapathId;

		public void run() {
			System.out.printf("(Java) [StatsGet::run] send_table_stats_request\n");
			Jaxon.send_table_stats_request(this.datapathId);
		}

		public StatsGet(Openflow.Datapathid dpid) {
			this.datapathId = dpid;
		}
	}
}
