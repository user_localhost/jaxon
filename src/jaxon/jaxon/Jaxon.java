package jaxon;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.StringArray;
import com.sun.jna.WString;
import com.sun.jna.Callback;
import com.sun.jna.Structure;
import com.sun.jna.Pointer;

import java.io.*;
import java.lang.System;
 
public class Jaxon {

	public interface CBDatapathJoin extends Callback {
		public int callback(Request req);
	}

	public interface CBDatapathLeave extends Callback {
		public int callback(Request req);
	}

	public interface CBPacketIn extends Callback {
		public int callback(RequestPacketIn req);
	}

	public interface CBTableStats extends Callback {
		public int callback(RequestTableStatsIn req);
	}

	public interface CBPortStats extends Callback {
		public int callback(RequestPortStatsIn req);
	}

	public interface CBAggregateStats extends Callback {
		public int callback(RequestAggregateStatsIn req);
	}

	public interface CBFlowStats extends Callback {
		public int callback(RequestFlowStatsIn req);
	}

	public interface CBError extends Callback {
		public int callback(RequestError req);
	}

	/* This datastructure fulfill a role of bridge between Jaxon and NOX
	 * to install microflow into openflow-switch. */
	public static class Request extends Structure {
		public Openflow.OfpFlowMod.ByReference ofm;
		public Openflow.Datapathid dpid;
		public Openflow.Flow.ByReference flow;
	}

	public static class RequestError extends Structure {
		public Openflow.OfpFlowMod.ByReference ofm;
		public Openflow.Datapathid dpid;
		public Error.ByReference error;
	}

	public static class RequestTableStatsIn extends Structure {
		public Openflow.OfpFlowMod.ByReference ofm;
		public Openflow.Datapathid dpid;
		public TableStats.ByReference table;
	}

	public static class RequestPortStatsIn extends Structure {
		public Openflow.OfpFlowMod.ByReference ofm;
		public Openflow.Datapathid dpid;
		public PortStats.ByReference port;
	}

	public static class RequestAggregateStatsIn extends Structure {
		public Openflow.OfpFlowMod.ByReference ofm;
		public Openflow.Datapathid dpid;
		public AggregateStats.ByReference aggr;
	}

	public static class RequestFlowStatsIn extends Structure {
		public Openflow.OfpFlowMod.ByReference ofm;
		public Openflow.Datapathid dpid;
		public FlowStats.ByReference flow;
	}

	public static class RequestPacketIn extends Structure {
		public Openflow.OfpFlowMod.ByReference ofm;
		public Openflow.Datapathid dpid;
		public Openflow.Flow.ByReference flow;
	}
	
	public static class TableStatsIn extends Structure {
		public static class ByReference extends TableStatsIn implements Structure.ByReference { }

		public int table_id;
		public String name;
		public int max_entries;
		public int active_count;
		public long lookup_count;
		public long matched_count;
	}

	public static class TableStats extends Structure {
		public static class ByReference extends TableStats implements Structure.ByReference { }

		public TableStatsIn.ByReference tsArray;
		public int len;

		public TableStatsIn[] toArray() {
			return (TableStatsIn[]) tsArray.toArray(len);
		}
	}

	public static class Error extends Structure {
		public static class ByReference extends Error implements Structure.ByReference { }

		public short type;
		public short code;
	}
	
	public static class PortStatsIn extends Structure {
		public static class ByReference extends PortStatsIn implements Structure.ByReference { }

		public short port_no;
		public long rx_packets;
		public long tx_packets;
		public long rx_bytes;
		public long tx_bytes;
		public long rx_dropped;
		public long tx_dropped;
		public long rx_errors;
		public long tx_errors;
		public long rx_frame_err;
		public long rx_over_err;
		public long rx_crc_err;
		public long collisions;
	}

	public static class PortStats extends Structure {
		public static class ByReference extends PortStats implements Structure.ByReference { }

		public PortStatsIn.ByReference psArray;
		public int len;

		public PortStatsIn[] toArray() {
			return (PortStatsIn[]) psArray.toArray(len);
		}
	}

	public static class AggregateStats extends Structure {
		public static class ByReference extends AggregateStats implements Structure.ByReference { }

		public long packet_count;
		public long byte_count;
		public int flow_count;
	}
	
	public static class FlowStatsIn extends Structure {
		public static class ByReference extends FlowStatsIn implements Structure.ByReference { }
		
		public Openflow.OfpActionHeader.ByReference ahList;
		public int len;

		public Openflow.OfpActionHeader[] toArray() {
			return (Openflow.OfpActionHeader[]) ahList.toArray(len);
		}

		//public Openflow.OfpActionHeader get(int offset) {
			//Openflow.OfpActionHeader ret = null;
			//Openflow.OfpActionHeader[] array = 
				//(Openflow.OfpActionHeader[]) ahList.toArray(len);

			//if(array != null) {
				//ret = array[offset];
			//}

			//return ret;
		//}
	}

	public static class FlowStats extends Structure {
		public static class ByReference extends FlowStats implements Structure.ByReference { }

		public FlowStatsIn.ByReference fsArray;
		public int len;

		public FlowStatsIn[] toArray() {
			return (FlowStatsIn[]) fsArray.toArray(len);
		}
	}
	
	public static class FlowAction extends Structure {
		public short type;
		public short vlan_id;
		public short vlan_pcp;
		public byte[] dl_addr = new byte[Openflow.OFP_ETH_ALEN];
		public short dl_type;
		public byte nw_proto;
		public int nw_addr;
		public short tp_port;
		public short of_port;
		public byte nw_tos;

		public static final int SIZE = 19;

		public FlowAction() {
			super();
		}

		public FlowAction(Pointer pointer) {
			super(pointer);
		}
	}

	public interface CLibrary extends Library {
		CLibrary INSTANCE = (CLibrary) Native.loadLibrary("jaxoncore", CLibrary.class);
	
		void do_nox_init(int argc, String[] args);
		int set_handler_datapath_join(CBDatapathJoin cb);
		int set_handler_datapath_leave(CBDatapathLeave cb);
		int set_handler_packet_in(CBPacketIn cb);
		int set_handler_table_stats_in(CBTableStats cb);
		int set_handler_port_stats_in(CBPortStats cb);
		int set_handler_aggregate_stats_in(CBAggregateStats cb);
		int set_handler_flow_stats_in(CBFlowStats cb);
		int set_handler_flow_stats_in(CBError cb);
		void install_datapath_flow(RequestPacketIn req, FlowAction[] obj, int len);
		void install_datapath_flow(Request req, FlowAction[] obj, int len);
		void send_table_stats_request(Openflow.Datapathid dpid);
		void send_port_stats_request(Openflow.Datapathid dpid, short port);
		void send_aggregate_stats_request(Openflow.Datapathid dpid, Openflow.OfpMatch match, byte table_id);
		void send_flow_stats_request(Openflow.Datapathid dpid, Openflow.OfpMatch match, byte table_id);
		void send_packet(RequestPacketIn req, short out_port);
		int conv_aton(String ip_str);
		void dump_ofm(Openflow.OfpFlowMod data);
		void dump_request(final Request req, final String str);
		void dump_flow_obj(final Openflow.Flow data, final String str);
		void dump_flow_action(final FlowAction[] data, int len);
	}

	public void Dump(Openflow.OfpFlowMod data) {
		CLibrary.INSTANCE.dump_ofm(data);
	}

	public void Dump(Request req) {
		CLibrary.INSTANCE.dump_request(req, "from Java");
	}

	public void Dump(Openflow.Flow data) {
		CLibrary.INSTANCE.dump_flow_obj(data, "from Java");
	}

	public void Dump(FlowAction[] data) {
		CLibrary.INSTANCE.dump_flow_action(data, data.length);
	}

	public void run(String[] args) {
		String[] nargs = new String[args.length + 1];
		nargs[0] = "./this";
		System.arraycopy(args, 0, nargs, 1, args.length);

		CLibrary.INSTANCE.do_nox_init(nargs.length, nargs);
	}

	/* Event register processing */
	public void setHandler(CBDatapathJoin cb) {
		CLibrary.INSTANCE.set_handler_datapath_join(cb);
	}
	
	public void setHandler(CBDatapathLeave cb) {
		CLibrary.INSTANCE.set_handler_datapath_leave(cb);
	}

	public void setHandler(CBPacketIn cb) {
		CLibrary.INSTANCE.set_handler_packet_in(cb);
	}

	public void setHandler(CBTableStats cb) {
		CLibrary.INSTANCE.set_handler_table_stats_in(cb);
	}

	public void setHandler(CBPortStats cb) {
		CLibrary.INSTANCE.set_handler_port_stats_in(cb);
	}

	public void setHandler(CBAggregateStats cb) {
		CLibrary.INSTANCE.set_handler_aggregate_stats_in(cb);
	}

	public void setHandler(CBFlowStats cb) {
		CLibrary.INSTANCE.set_handler_flow_stats_in(cb);
	}

	public void setHandler(CBError cb) {
		CLibrary.INSTANCE.set_handler_flow_stats_in(cb);
	}

	public void installFlow(RequestPacketIn req, FlowAction[] obj) {
		CLibrary.INSTANCE.install_datapath_flow(req, obj, obj.length);
	}

	public void installFlow(Request req, FlowAction[] obj) {
		CLibrary.INSTANCE.install_datapath_flow(req, obj, obj.length);
	}

	public void sendPacket(RequestPacketIn req, short out_port) {
		CLibrary.INSTANCE.send_packet(req, out_port);
	}
	
	public static FlowAction[] getFlowAction(int len) {
		return (FlowAction[]) new FlowAction().toArray(len);
	}

	public static int inet_aton(String ip_str) {
		return CLibrary.INSTANCE.conv_aton(ip_str);
	}

	public static void send_table_stats_request(Openflow.Datapathid dpid) {
		CLibrary.INSTANCE.send_table_stats_request(dpid);
	}

	public static void send_port_stats_request(Openflow.Datapathid dpid, short port) {
		CLibrary.INSTANCE.send_port_stats_request(dpid, port);
	}

	public static void send_aggregate_stats_request(Openflow.Datapathid dpid, Openflow.OfpMatch match, byte table_id) {
		CLibrary.INSTANCE.send_aggregate_stats_request(dpid, match, table_id);
	}

	public static void send_flow_stats_request(Openflow.Datapathid dpid, Openflow.OfpMatch match, byte table_id) {
		CLibrary.INSTANCE.send_flow_stats_request(dpid, match, table_id);
	}
}
