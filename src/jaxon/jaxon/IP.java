package jaxon;

public class IP {
	public static class Proto {
		public static final byte IP = 0;        // Dummy protocol for TCP
		public static final byte HOPOPTS = 0;   // IPv6 Hop-by-Hop options.  
		public static final byte ICMP = 1;      // Internet Control Message Protocol.  
		public static final byte IGMP = 2;      // Internet Group Management Protocol. 
		public static final byte IPIP = 4;      // IPIP tunnels (older KA9Q tunnels use 94).  
		public static final byte TCP = 6;       // Transmission Control Protocol.  
		public static final byte EGP = 8;       // Exterior Gateway Protocol.  
		public static final byte PUP = 12;      // PUP protocol.  
		public static final byte UDP = 17;      // User Datagram Protocol.  
		public static final byte IDP = 22;      // XNS IDP protocol.  
		public static final byte TP = 29;       // SO Transport Protocol Class 4.  
		public static final byte IPV6 = 41;     // IPv6 header.  
		public static final byte ROUTING = 43;  // IPv6 routing header.  
		public static final byte FRAGMENT = 44; // IPv6 fragmentation header.  
		public static final byte RSVP = 46;     // Reservation Protocol.  
		public static final byte GRE = 47;      // General Routing Encapsulation.  
		public static final byte ESP_PROTO = 50;      // encapsulating security payload.  
		public static final byte AH = 51;       // authentication header.  
		public static final byte ICMPV6 = 58;   // ICMPv6.  
		public static final byte NONE = 59;     // IPv6 no next header.  
		public static final byte DSTOPTS = 60;  // IPv6 destination options.  
		public static final byte MTP = 92;      // Multicast Transport Protocol.  
		public static final byte ENCAP = 98;    // Encapsulation Header.  
		public static final byte PIM = 103;     // Protocol Independent Multicast.  
		public static final byte COMP = 108;    // Compression Header Protocol.  
		public static final byte SCTP = (byte)132;    // Stream Control Transmission Protocol.  
		public static final byte RAW = (byte)255;     // Raw IP packets.  
	}
}
