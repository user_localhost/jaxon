package jaxon;

import com.sun.jna.Structure;
import com.sun.jna.Pointer;
import java.io.*;

public class Event {
	public static class Disposition {
		public static final int CONTINUE = 0;
		public static final int STOP = 1;
	}
}
