package jaxon;

public class Ethernet {
	public static final int ETH_TYPE_PUP = 0x0200;
	public static final int ETH_TYPE_IP  = 0x0800;
	public static final int ETH_TYPE_ARP  = 0x0806;
	public static final int ETH_TYPE_REVARP  = 0x8035;
	public static final int ETH_TYPE_VLAN  = 0x8100;
	public static final int ETH_TYPE_IPV6  = 0x86dd;
	public static final int ETH_TYPE_LLDP  = 0x88cc;
	public static final int ETH_TYPE_PAE  = 0x888e;
	public static final int ETH_TYPE_ETH2_CUTOFF  = 0x0600;
}
