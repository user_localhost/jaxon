package jaxon;

import java.nio.*;

public class Util {
	private static final int BUFFER_SIZE = 10;
	private static ByteBuffer bb = ByteBuffer.allocate(BUFFER_SIZE);

	public static short htons(short value) {
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putShort(0, value);
		bb.order(ByteOrder.BIG_ENDIAN);

		return bb.getShort(0);
	}

	public static short htons(int value) {
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putShort(0, (short)value);
		bb.order(ByteOrder.BIG_ENDIAN);

		return bb.getShort(0);
	}

	public static int htonl(int value) {
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putInt(0, value);
		bb.order(ByteOrder.BIG_ENDIAN);

		return bb.getInt(0);
	}

	public static short ntohs(short value) {
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putShort(0, value);
		bb.order(ByteOrder.LITTLE_ENDIAN);

		return bb.getShort(0);
	}

	public static int ntohl(int value) {
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(0, value);
		bb.order(ByteOrder.LITTLE_ENDIAN);

		return bb.getInt(0);
	}
}
