package jaxon;

import com.sun.jna.Structure;
import com.sun.jna.Pointer;
import java.io.*;

public class Openflow {
	/* Bytes in an Ethernet address. */
	public static final int OFP_ETH_ALEN = 6;

	/* Fields to match against flows */
	public static class OfpMatch extends Structure {
		public int wildcards;
		public short in_port;
		public byte[] dl_src = new byte[OFP_ETH_ALEN];
		public byte[] dl_dst = new byte[OFP_ETH_ALEN];
		public short dl_vlan;
		public byte dl_vlan_pcp;
		public byte[] pad1 = new byte[1];
		public short dl_type;
		public byte nw_tos;
		public byte nw_proto;

		public byte[] pad2 = new byte[2];
		public int nw_src;
		public int nw_dst;
		public short tp_src;
		public short tp_dst;

		public OfpMatch() {
			super();
		}
	}

	public static class Datapathid extends Structure {
		public long id;
	}

	public static class OfpFlowWildcards extends Structure {
		public static final int OFPFW_IN_PORT  = 1 << 0;  /* Switch input port. */
		public static final int OFPFW_DL_VLAN  = 1 << 1;  /* VLAN id. */
		public static final int OFPFW_DL_SRC   = 1 << 2;  /* Ethernet source address. */
		public static final int OFPFW_DL_DST   = 1 << 3;  /* Ethernet destination address. */
		public static final int OFPFW_DL_TYPE  = 1 << 4;  /* Ethernet frame type. */
		public static final int OFPFW_NW_PROTO = 1 << 5;  /* IP protocol. */
		public static final int OFPFW_TP_SRC   = 1 << 6;  /* TCP/UDP source port. */
		public static final int OFPFW_TP_DST   = 1 << 7;  /* TCP/UDP destination port. */

		/* IP source address wildcard bit count.  0 is exact match, 1 ignores the
		 * LSB, 2 ignores the 2 least-significant bits, ..., 32 and higher wildcard
		 * the entire field.  This is the *opposite* of the usual convention where
		 * e.g. /24 indicates that 8 bits (not 24 bits) are wildcarded. */
		public static final int OFPFW_NW_SRC_SHIFT = 8;
		public static final int OFPFW_NW_SRC_BITS = 6;
		public static final int OFPFW_NW_SRC_MASK = ((1 << OFPFW_NW_SRC_BITS) - 1) << OFPFW_NW_SRC_SHIFT;
		public static final int OFPFW_NW_SRC_ALL = 32 << OFPFW_NW_SRC_SHIFT;

		/* IP destination address wildcard bit count.  Same format as source. */
		public static final int OFPFW_NW_DST_SHIFT = 14;
		public static final int OFPFW_NW_DST_BITS = 6;
		public static final int OFPFW_NW_DST_MASK = ((1 << OFPFW_NW_DST_BITS) - 1) << OFPFW_NW_DST_SHIFT;
		public static final int OFPFW_NW_DST_ALL = 32 << OFPFW_NW_DST_SHIFT;

		public static final int OFPFW_DL_VLAN_PCP = 1 << 20;  /* VLAN priority. */
		public static final int OFPFW_NW_TOS = 1 << 21;  /* IP ToS (DSCP field, 6 bits). */

		/* Wildcard all fields. */
		public static final int OFPFW_ALL = ((1 << 22) - 1);
	}

	/* Header on all OpenFlow packets. */
	public static class OfpHeader extends Structure {
		public byte version;
		public byte type;
		public short length;
		public int xid;
	}

	public static class OfpFlowMod extends Structure {
		public static class ByReference extends OfpFlowMod implements Structure.ByReference { }

		public OfpHeader header;
		public OfpMatch match;
		public long cookie;

		public short command;
		public short idle_timeout;
		public short hard_timeout;
		public short priority;
		public int buffer_id;

		public short out_port;
		public short flags;
	}

	public static class Flow extends Structure {
		public static class ByReference extends Flow implements Structure.ByReference { }

		public byte invalid;
		public short in_port;
		public short dl_vlan;
		public byte dl_vlan_pcp;
		public Ethernetaddr dl_src;
		public Ethernetaddr dl_dst;
		public short dl_type;
		public int nw_src;
		public int nw_dst;
		public byte nw_proto;
		public byte nw_tos;
		public short tp_src;
		public short tp_dst;
		public long cookie;
	}

	public static class OfpPort extends Structure {
		public static final short OFPP_MAX = (short) 0xff00;
		public static final short OFPP_IN_PORT = (short) 0xfff8;
		public static final short OFPP_TABLE = (short) 0xfff9;
		public static final short OFPP_NORMAL = (short) 0xfffa;
		public static final short OFPP_FLOOD = (short) 0xfffb;
		public static final short OFPP_ALL = (short) 0xfffc;
		public static final short OFPP_CONTROLLER = (short) 0xfffd;
		public static final short OFPP_LOCAL = (short) 0xfffe;
		public static final short OFPP_NONE = (short) 0xffff;
	}
	
	public static class OfpActionType extends Structure {
		public static final short OFPAT_OUTPUT = 0;           /* Output to switch port. */
		public static final short OFPAT_SET_VLAN_VID = 1;     /* Set the 802.1q VLAN id. */
		public static final short OFPAT_SET_VLAN_PCP = 2;     /* Set the 802.1q priority. */
		public static final short OFPAT_STRIP_VLAN = 3;       /* Strip the 802.1q header. */
		public static final short OFPAT_SET_DL_SRC = 4;       /* Ethernet source address. */
		public static final short OFPAT_SET_DL_DST = 5;       /* Ethernet destination address. */
		public static final short OFPAT_SET_NW_SRC = 6;       /* IP source address. */
		public static final short OFPAT_SET_NW_DST = 7;       /* IP destination address. */
		public static final short OFPAT_SET_NW_TOS = 8;       /* IP ToS (DSCP field; 6 bits). */
		public static final short OFPAT_SET_TP_SRC = 9;       /* TCP/UDP source port. */
		public static final short OFPAT_SET_TP_DST = 10;      /* TCP/UDP destination port. */
    public static final short OFPAT_ENQUEUE = 11;         /* Output to queue.  */
    public static final short OFPAT_VENDOR = (short) 0xffff;
	}

	public static class Ethernetaddr extends Structure {
		public byte[] octet = new byte[OFP_ETH_ALEN];
	}

	public static class OfpActionOutput extends Structure {
		public static class ByReference extends OfpActionOutput implements Structure.ByReference { }

		public short type;
		public short len;
		public short port;
		public short max_len;
	}

	public static class OfpActionVlanVid extends Structure {
		public static class ByReference extends OfpActionVlanVid implements Structure.ByReference { }

		public short type;
		public short len;
		public short vlan_id;
		public byte[] pad = new byte[2];
	}

	public static class OfpActionVlanPCP extends Structure {
		public static class ByReference extends OfpActionVlanPCP implements Structure.ByReference { }

		public short type;
		public short len;
		public byte vlan_pcp;
		public byte[] pad = new byte[3];
	}

	public static class OfpActionDLAddr extends Structure {
		public static class ByReference extends OfpActionDLAddr implements Structure.ByReference { }

		public short type;
		public short len;
		public byte[] dl_addr = new byte[OFP_ETH_ALEN];
		public byte[] pad = new byte[6];
	}

	public static class OfpActionNWAddr extends Structure {
		public static class ByReference extends OfpActionNWAddr implements Structure.ByReference { }

		public short type;
		public short len;
		public int nw_addr;
	}

	public static class OfpActionTPPort extends Structure {
		public static class ByReference extends OfpActionTPPort implements Structure.ByReference { }

		public short type;
		public short len;
		public short tp_port;
		public byte[] pad = new byte[2];
	}

	public static class OfpActionNWTOS extends Structure {
		public static class ByReference extends OfpActionNWTOS implements Structure.ByReference { }

		public short type;
		public short len;
		public byte nw_tos;
		public byte[] pad = new byte[3];
	}

	public static class OfpActionVendorHeader extends Structure {
		public static class ByReference extends OfpActionVendorHeader implements Structure.ByReference { }

		public short type;
		public short len;
		public int vendor;
	}

	public static class OfpActionHeader extends Structure {
		public static class ByReference extends OfpActionHeader implements Structure.ByReference { }

		public short type;
		public short len;
		public byte[] pad = new byte[4];
	}
}
