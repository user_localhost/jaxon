#include <iostream>
#include <string.h>
#include <signal.h>

#include <boost/bind.hpp>
#include <boost/shared_array.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

#include "builtin/event-dispatcher-component.hh"

#include "nox.hh"
#include "netinet++/datapathid.hh"
#include "threads/impl.hh"

#include "flow-stats-in.hh"
#include "datapath-join.hh"
#include "datapath-leave.hh"
#include "packet-in.hh"
#include "table-stats-in.hh"
#include "port-stats-in.hh"
#include "aggregate-stats-in.hh"

#include "event-dispatcher.hh"
#include "flow-mod-event.hh"
#include "error-event.hh"
#include "assert.hh"
#include "openflow-default.hh"
#include "packets.h"

#include "kernel.hh"
#include "component.hh"
#include "static-deployer.hh"

#include "jaxon_core.hh"

#include "threads/cooperative.hh"
#include "netapps/jaxon/jaxon.hh"

using namespace vigil;
using namespace std;

static void set_action_header(ofp_action_header *, const struct flow_action *);
static void mymemcpy(uint8_t *, uint8_t *, int);
static int send_stats_request(const vigil::datapathid, ofp_stats_types, const uint8_t*, size_t);
static void init_handler(void);
static void set_default_handler(int);

int libmain(int, char **);

extern "C" {

	void do_nox_init(int argc, char **argv) {
		init_handler();

		libmain(argc, argv);
	}

	void set_handler_datapath_join(int (*event)(struct request *)) {
		event_handlers.datapath_join = event;
	}

	void set_handler_datapath_leave(int (*event)(struct request *)) {
		event_handlers.datapath_leave = event;
	}

	void set_handler_packet_in(int (*event)(struct request *)) {
		event_handlers.packet_in = event;
	}

	void set_handler_table_stats_in(int (*event)(struct request *)) {
		event_handlers.table_stats_in = event;
	}

	void set_handler_port_stats_in(int (*event)(struct request *)) {
		event_handlers.port_stats_in = event;
	}

	void set_handler_aggregate_stats_in(int (*event)(struct request *)) {
		event_handlers.aggregate_stats_in = event;
	}

	void set_handler_flow_stats_in(int (*event)(struct request *)) {
		event_handlers.flow_stats_in = event;
	}

	void set_handler_error(int (*event)(struct request *)) {
		event_handlers.flow_stats_in = event;
	}

	void install_datapath_flow(struct request *req, struct flow_action *flow_action, int len) {
		if(req == NULL) {
			printf("[install_datapath_flow] (FATAL ERROR) request object is NULL\n");
			return;
		}

		if(flow_action != NULL) {
			uint16_t size = sizeof(ofp_flow_mod) + sizeof(ofp_action_header) * len;
			boost::shared_array<uint8_t> raw_data(new uint8_t[size]);
			ofp_flow_mod *ofm = (ofp_flow_mod *) raw_data.get();
			ofp_action_header *action_header = ofm->actions;
			int i, err;

			/* copy ofp_flow_mod object */
			mymemcpy((uint8_t *)ofm, (uint8_t *)req->ofm, sizeof(ofp_flow_mod));

			/* re-setting value */
			ofm->header.length = htons(size);

			for(i=0; i<len; i++) {
				set_action_header(&action_header[i], &flow_action[i]);
			}

			dump_ofm(ofm);

			err = vigil::nox::send_openflow_command(req->datapath_id, &ofm->header, false);
		}
	}

	void send_table_stats_request(const vigil::datapathid dpid) {
		send_stats_request(dpid, OFPST_TABLE, 0, 0);
	}

	void send_port_stats_request(const vigil::datapathid dpid, uint16_t port) {
    ofp_port_stats_request psr;
    psr.port_no = htons(port);
    send_stats_request(dpid, OFPST_PORT, (const uint8_t*)&psr, sizeof(struct ofp_port_stats_request));
	}

	void send_aggregate_stats_request(const vigil::datapathid dpid, const struct ofp_match& match, uint8_t table_id) {
    ofp_aggregate_stats_request asr;
    asr.table_id = table_id;
    asr.match    = match;
    asr.out_port = htons(OFPP_NONE);
    send_stats_request(dpid, OFPST_AGGREGATE, (const uint8_t*)&asr, sizeof(struct ofp_aggregate_stats_request));
	}
	
	void send_flow_stats_request(const vigil::datapathid dpid, const struct ofp_match& match, uint8_t table_id) {
    ofp_flow_stats_request fsr;
    fsr.table_id = table_id;
    fsr.match    = match;
    fsr.out_port = htons(OFPP_NONE);
    send_stats_request(dpid, OFPST_FLOW, (const uint8_t*)&fsr, sizeof(struct ofp_flow_stats_request));
	}

	void send_packet(struct request *req, short out_port) {
		vigil::Flow *flow = (vigil::Flow *)req->data;

		if(req == NULL) {
			return;
		}

		if(req->buffer_id == ((uint32_t) -1)) {
			vigil::nox::send_openflow_packet_out(
					req->datapath_id, *(req->buf), OFPP_FLOOD, flow->in_port, false);
		} else {
			vigil::nox::send_openflow_packet_out(
					req->datapath_id, req->buffer_id, OFPP_FLOOD, flow->in_port, false);
		}
	}

	/* These routines are for Java component */
	short local_ntohs(short val) {
		return ntohs(val);
	}
	
	short local_htons(short val) {
		return htons(val);
	}

	uint32_t conv_aton(char *ip_str) {
		struct in_addr addr;
		uint32_t ret = 0;

		if(inet_aton(ip_str, &addr) != 0) {
			ret = addr.s_addr;
		}

		return ret;
	}

#ifdef __DEBUG__
	static void dump_obj(short *data, int length, const char *label) {
		int i;
		for(i=0; i<(length / sizeof(short)); i++) {
			if(i % 8 == 0) {
				printf("\n[%s] <0x%05x> ", label, i * sizeof(short));
			}

			printf("%04x ", data[i] & 0xffff);
		}

		printf("\n");
	}

	void dump_ofm(ofp_flow_mod *ofm) {

    printf("[dump_ofm] ofm->header.version    : 0x%02x\n", ofm->header.version & 0xff);
    printf("[dump_ofm] ofm->header.type       : 0x%02x\n", ofm->header.type & 0xff);
    printf("[dump_ofm] ofm->header.length     : 0x%04x\n", ofm->header.length & 0xffff);
    printf("[dump_ofm] ofm->header.xid        : 0x%08x\n", ofm->header.xid);
    printf("[dump_ofm] ofm->match.wildcards   : 0x%08x\n", ofm->match.wildcards);
    printf("[dump_ofm] ofm->match.in_port     : 0x%04x\n", ofm->match.in_port & 0xffff);
    printf("[dump_ofm] ofm->match.dl_vlan     : 0x%04x\n", ofm->match.dl_vlan & 0xffff);
    printf("[dump_ofm] ofm->match.dl_vlan_pcp : 0x%02x\n", ofm->match.dl_vlan_pcp & 0xff);
    printf("[dump_ofm] ofm->match.dl_type     : 0x%04x\n", ofm->match.dl_type & 0xffff);
    printf("[dump_ofm] ofm->match.nw_tos      : 0x%02x\n", ofm->match.nw_tos & 0xff);
    printf("[dump_ofm] ofm->match.nw_proto    : 0x%02x\n", ofm->match.nw_proto & 0xff);
    printf("[dump_ofm] ofm->match.nw_src      : 0x%08x\n", ofm->match.nw_src);
    printf("[dump_ofm] ofm->match.nw_dst      : 0x%08x\n", ofm->match.nw_dst);
    printf("[dump_ofm] ofm->match.tp_src      : 0x%04x\n", ofm->match.tp_src & 0xffff);
    printf("[dump_ofm] ofm->match.tp_dst      : 0x%04x\n", ofm->match.tp_dst & 0xffff);
    printf("[dump_ofm] ofm->cookie            : 0x%x\n", ofm->cookie);
    printf("[dump_ofm] ofm->command           : 0x%x\n", ofm->command);
    printf("[dump_ofm] ofm->idle_timeout      : 0x%x\n", ofm->idle_timeout);
    printf("[dump_ofm] ofm->hard_timeout      : 0x%x\n", ofm->hard_timeout);
    printf("[dump_ofm] ofm->out_port          : 0x%x\n", ofm->out_port);
    printf("[dump_ofm] ofm->flags             : 0x%x\n", ofm->flags);
		
		ofp_action_output *action = (ofp_action_output*) (ofm->actions);
		if(action != NULL) {
	    printf("[dump_ofm] action->type           : 0x%04x\n", action->type & 0xffff);
	    printf("[dump_ofm] action->len            : 0x%04x\n", action->len & 0xffff);
	    printf("[dump_ofm] action->port           : 0x%04x\n", action->port & 0xffff);
	    printf("[dump_ofm] action->max_len        : 0x%04x\n", action->max_len & 0xffff);
		}

		dump_obj((short *)ofm, sizeof(ofp_flow_mod), "dump_ofm\0");
	}
	
	void dump_request(const struct request *req, const char *str) {
		printf("[dump_request] === %s ===\n", str);
		dump_obj((short *)req, sizeof(struct request), "[dump_request\0]");
	}

	void dump_flow_obj(const vigil::Flow *flow, const char *str) {
		printf("[dump_flow_obj] === %s === (0x%x)\n", str, flow);
		dump_obj((short *)flow, sizeof(vigil::Flow), "dump_flow_obj\0");
	}
	
	void dump_flow_action(const struct flow_action *array, int len) {
		int i;

		printf("[dump_flow_action] array : 0x%x\n", (unsigned int) array);

		for(i=0; i<len; i++) {
			const struct flow_action attr = array[i];

			printf("[dump_flow_action] (%d) attr.type : 0x%04x\n", i, attr.type & 0xffff);
			printf("[dump_flow_action] (%d) attr.vlan_id : 0x%04x\n", i, attr.vlan_id & 0xffff);
			printf("[dump_flow_action] (%d) attr.vlan_pcp : 0x%04x\n", i, attr.vlan_pcp & 0xffff);
			printf("[dump_flow_action] (%d) attr.dl_type : 0x%04x\n", i, attr.dl_type & 0xffff);
			printf("[dump_flow_action] (%d) attr.nw_proto : 0x%02x\n", i, attr.nw_proto & 0xff);
			printf("[dump_flow_action] (%d) attr.nw_addr : 0x%08x\n", i, attr.nw_addr & 0xffffffff);
			printf("[dump_flow_action] (%d) attr.tp_port : 0x%08x\n", i, attr.tp_port & 0xffff);
			printf("[dump_flow_action] (%d) attr.of_port : 0x%04x\n", i, attr.of_port & 0xffff);
			printf("[dump_flow_action] (%d) attr.nw_tos : 0x%02x\n", i, attr.nw_tos & 0xff);
		}
	}

#else
	void dump_ofm(ofp_flow_mod *ofm) {}
	void dump_request(const struct request *req, const char *str) {}
	void dump_flow_obj(const vigil::Flow *flow, const char *str) {}
	void dump_flow_action(const struct flow_action *attr, int len) {}
#endif
}

static void set_action_header(ofp_action_header *header, const struct flow_action *flow_attr) {
	uint16_t type;

	if(header == NULL || flow_attr == NULL) {
		printf("[set_action_header] (ERROR) header is NULL\n");
		return ;
	}

	/* Here is common processing at each action type discribed as 'type' value */
	type = flow_attr->type;

	/* set each action type */
	header->type = htons(type);

	/* Here is specific processing at each action type */
	if(type == OFPAT_OUTPUT) {

		ofp_action_output *action = (ofp_action_output *) header;

		action->len = htons(sizeof(ofp_action_output));
		action->port = flow_attr->of_port;
		//action->port = htons(OFPP_NORMAL); // should be normal
		action->max_len = 0;

	} else if (type == OFPAT_SET_VLAN_VID) {
		ofp_action_vlan_vid *action = (ofp_action_vlan_vid *) header;
		
		action->len = htons(sizeof(ofp_action_vlan_vid));
		
		/* ============================================= */
		/* This has not been implemented completely, yet */
		/* ============================================= */

	} else if (type == OFPAT_SET_VLAN_PCP) {
		ofp_action_vlan_pcp *action = (ofp_action_vlan_pcp *) header;
	
		action->len = htons(sizeof(ofp_action_vlan_pcp));
		
		/* ============================================= */
		/* This has not been implemented completely, yet */
		/* ============================================= */

	} else if (type == OFPAT_SET_DL_SRC || type == OFPAT_SET_DL_DST) {

		ofp_action_dl_addr *action = (ofp_action_dl_addr *) header;
	
		action->len = htons(sizeof(ofp_action_dl_addr));
		mymemcpy((uint8_t *)action->dl_addr, (uint8_t *)flow_attr->dl_addr, OFP_ETH_ALEN);

	} else if (type == OFPAT_SET_NW_SRC || type == OFPAT_SET_NW_DST) {

		ofp_action_nw_addr *action = (ofp_action_nw_addr *) header;
	
		action->len = htons(sizeof(ofp_action_nw_addr));
		action->nw_addr = flow_attr->nw_addr;

	} else if (type == OFPAT_SET_TP_SRC || type == OFPAT_SET_TP_DST) {
		ofp_action_tp_port *action = (ofp_action_tp_port *) header;
	
		action->len = htons(sizeof(ofp_action_tp_port));
		action->tp_port = htons(flow_attr->tp_port);

	} else if (type == OFPAT_SET_NW_TOS) {
		ofp_action_nw_tos *action = (ofp_action_nw_tos *) header;
	
		action->len = htons(sizeof(ofp_action_nw_tos));
		
		/* ============================================= */
		/* This has not been implemented completely, yet */
		/* ============================================= */

	} else if (type == OFPAT_VENDOR) {
		ofp_action_vendor_header *action = (ofp_action_vendor_header *) header;
	
		action->len = htons(sizeof(ofp_action_vendor_header));
		
		/* ============================================= */
		/* This has not been implemented completely, yet */
		/* ============================================= */

	}
}

static void mymemcpy(uint8_t *dst, uint8_t *src, int len) {
	int i;

	for(i=0; i<len; i++) {
		dst[i] = src[i];
	}
}

static int send_stats_request(const vigil::datapathid dpid, ofp_stats_types type, const uint8_t* data, size_t dsize) {
	ofp_stats_request* osr = NULL;
	size_t msize = sizeof(ofp_stats_request) + dsize;
	boost::shared_array<uint8_t> raw_sr(new uint8_t[msize]);

	// Send OFPT_STATS_REQUEST
	osr = (ofp_stats_request*) raw_sr.get();
	osr->header.type    = OFPT_STATS_REQUEST;
	osr->header.version = OFP_VERSION;
	osr->header.length  = htons(msize);
	osr->header.xid     = 0;
	osr->type           = htons(type);
	osr->flags          = htons(0); /* CURRENTLY NONE DEFINED */

	if(data){
		::memcpy(osr->body, data, dsize );
	}

	//int error = c->send_openflow_command(datapathid::from_host(dpid), &osr->header, false);
	return vigil::nox::send_openflow_command(dpid, &osr->header, false);
}

static void init_handler(void) {
	for(int i=1; i<=SIGRTMAX; i++) {
		set_default_handler(i);
	}
}

static void set_default_handler(int sig_nr) {
	struct sigaction sa;

	sa.sa_handler = SIG_DFL;
	sigaction(sig_nr, &sa, NULL);
}
