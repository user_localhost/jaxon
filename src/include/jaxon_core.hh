#ifndef __WRAPPER_HH__
#define __WRAPPER_HH__

#include "openflow-default.hh"
#include "netinet++/datapathid.hh"
#include "flow.hh"

//#include "events.hh"
#include "netapps/jaxon/jaxon.hh"

#include <boost/shared_ptr.hpp>

#define TABLE_NAME_LEN 64

namespace vigil {

	struct Jaxon {
	public:
		Jaxon() {}

		void config();

		Disposition handle_flow_stats_in(const Event& e);
	};
}

extern "C" {
	void do_nox_init(int, char **);
	void set_handler_datapath_join(int (*)(struct request *));
	void set_handler_datapath_leave(int (*)(struct request *));
	void set_handler_packet_in(int (*)(struct request *));
	void set_handler_table_stats_in(int (*)(struct request *));
	void set_handler_port_stats_in(int (*)(struct request *));
	void set_handler_aggregate_stats_in(int (*)(struct request *));
	void set_handler_flow_stats_in(int (*)(struct request *));
	void set_handler_error(int (*)(struct request *));

	void install_datapath_flow(struct request*, struct flow_action *, int);
	void send_table_stats_request(const vigil::datapathid);
	void send_port_stats_request(const vigil::datapathid, uint16_t);
	void send_aggregate_stats_request(const vigil::datapathid, const struct ofp_match&, uint8_t);
	void send_flow_stats_request(const vigil::datapathid, const struct ofp_match&, uint8_t);
	void send_packet(struct request *, short);
	
	/* These are for Java component */
	short local_ntohs(short);
	short local_htons(short);

	uint32_t conv_aton(char *);

	/* here is debug processing */
	void dump_ofm(ofp_flow_mod *);
	void dump_request(const struct request *, const char *);
	void dump_flow_obj(const vigil::Flow *, const char *);
	void dump_flow_action(const struct flow_action *, int);
}

#endif
