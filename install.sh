#!/bin/sh

# Author : Hiroyasu OHYAMA (OSSS lab. / ariel-network Inc.)
# Last-update : 2012/02/26
#
# Description:
#		This script is installer of Jaxon.

dirpath=$(dirname $0)
noxpath=$1
temporarypath=".jaxon_tmp"

if test -d ${noxpath}
then
	jaxon_make_am=${dirpath}/src/Makefile.am
	nox_make_am=${noxpath}/src/Makefile.am

	local_component_dir=${dirpath}/src/nox/netapps/jaxon/
	nox_component_dir=${noxpath}/src/nox/netapps/jaxon/

	# apply Jaxon NOX-component
	if test ! -d ${nox_component_dir}
	then
		echo "cd ${noxpath}/src/nox/netapps; ../../scripts/nox-new-c-app.py jaxon" | bash

		cp -r ${local_component_dir}/jaxon* ${nox_component_dir}
	fi

	# adding Jaxon java samples
	if test -d ${noxpath}/src/
	then
		cp -r ${dirpath}/src/jaxon ${noxpath}/src/
		cp -r ${dirpath}/src/include ${noxpath}/src/
		cp ${dirpath}/src/jaxon_core.cc ${noxpath}/src/
	else
		echo "The ditected directory of "${noxpath}" is invalid as a NOX directory. Please ditect a correct one."
		exit 1
	fi

	# add class files to ${noxpath}/src/jaxon/Makefile.am
	writetmp="${noxpath}/src/jaxon/.files"
	tmpfile="${noxpath}/src/jaxon/.Makefile.am.tmp"
	
	ls ${noxpath}/src/jaxon/*.java | while read file
	do
		result="${result} $(basename ${file})"
	
		echo ${result} > ${writetmp}
	done
	
	sed -e "s/^\(java_JAVA = \).*$\|^\(EXTRA_DIST = \).*$/\1\2$(cat $writetmp)/" < ${noxpath}/src/jaxon/Makefile.am > ${tmpfile}
	
	mv ${tmpfile} ${noxpath}/src/jaxon/Makefile.am
	rm ${writetmp}

	# modifing Makefile.am
	if test -e ${nox_make_am}
	then

		if test -z "$(grep jaxon ${nox_make_am})"
		then
			sed -e "s/^int main(/int libmain(/" ${noxpath}/src/nox_main.cc > ${noxpath}/src/libnox_main.cc
			sed -e "s/nox_core_/libnox_core_la_/" \
				-e "s/nox_main\.cc/libnox_main\.cc/" \
				-e "s/_LDADD/_LIBADD/" \
				${nox_make_am} > ${temporarypath}; mv ${temporarypath} ${nox_make_am} 

			cat ${nox_make_am} ${jaxon_make_am} | awk '
			{
				if(/^SUBDIR/ && ! /jaxon/) {
					print $0, "jaxon"
				} else {
					print $0
				}
			}
			' > ${temporarypath}
			
			mv ${nox_make_am} ${nox_make_am}.bck
			mv ${temporarypath} ${nox_make_am}

			cp ${dirpath}/src/new_nox_main.cc ${noxpath}/src/nox_main.cc
		else
			echo "Jaxon has already been installed. This script do nothing."
			exit 1
		fi

	else
		echo "The ditected directory of "${noxpath}" is invalid as a NOX directory. Please ditect a correct one."
		exit 1
	fi
fi
