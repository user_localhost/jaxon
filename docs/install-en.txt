Preparing dependent environment

1. install JNA (Java Native Access) Libraries
  - download it from here and uncompress. the variable of JNA_TOP means the direcoty path to which jna will be uncompressed.
    e.g.
    $ cd ${JNA_TOP}
    $ wget https://github.com/twall/jna/zipball/master -O jna_master.zip
    $ unzip jna_master.zip

  - add the path of "${JNA_TOP}/dist/jna.jar" to the environment variable of CLASSPATH
    e.g.
    $ export CLASSPATH:${JNA_TOP}/${JNA_TOP}/dist/jna.jar

2. getting NOX
  - get it from git-repository. the variable of NOX_TOP means the directory path to which NOX will be installed.
    e.g.
    $ cd ${NOX_TOP}
    $ git clone git://noxrepo.org/nox

Install Jaxon

1. Download and Appending

	- Download Jaxon from Download page and uncompress it. The variable of JAXON_TOP means the directory path to which NOX will be uncompressed
    In this sequence, Jaxon append NOX insaller to Jaxon's one. The script of install.sh requires the directory of NOX installed.
  
    $ ${JAXON_TOP}/install.sh ${NOX_TOP}
  
2. building NOX
  - After appending, NOX builder work with Jaxon. So you build NOX normally.

    e.g.
    $ cd ${NOX_TOP}
    $ ./boot.sh
    $ cd build
    $ ../configure
    $ make -j 5

3. Execution
  - You can use Jaxon as a OpenFlow controller from your Java-Program.
    But the interface is common that is "jaxon_core". To start it is following. 

    $ cd ${NOX}/build/src
    $ ./jaxon_core YourClassfileName -v -i ptcp::

    The Usage of it is.
    
    $ ./jaxon_core JaxonComponent [options]

    The first argument of "JaxonComponent" is java class file that user make. The way of how to make Jaxon Component and build it is written in "How to make" page.
    And after the first two of arguments, "options", are the same of NOX. These are to flow through to NOX's one.
